(ns banksmart.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [banksmart.customer.customer :as customer]
            [ring.middleware.json :refer [wrap-json-response wrap-json-body]]))

(defroutes app-routes
  (GET "/" [] "Bank Smart - Wellcome")
  (GET "/customer" [] 
    (customer/select-customer))
  (route/not-found "Not-found"))
  
(def app
  (-> app-routes
      wrap-json-response
      wrap-json-body))
