(ns banksmart.customer.services
  (:require [ring.middleware.json :as middleware]
            [ring.util.response :refer [response content-type]]
            [cheshire.core :refer :all]
            [compojure.core :refer :all]
            [banksmart.bank :as bank]))


(defn json [form]
  (-> form
      encode
      response
      (content-type "application/json; charset=utf-8")))

(defn add-account-transaction [account-number description amount date]
  (json (bank/add-account-transaction :account-number account-number :description description :amount :date date) ))