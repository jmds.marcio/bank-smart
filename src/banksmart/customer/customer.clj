(ns banksmart.customer.customer
  (:require [ring.middleware.json :as middleware]
            [ring.util.response :refer [response content-type]]
            [cheshire.core :refer :all]
            [compojure.core :refer :all]))

(def customer {0 {:name "Joseph" :doc 25232142521 :email "jmds.marcio@gmail.com" }})

(defn json [form]
  (-> form
      encode
      response
      (content-type "application/json; charset=utf-8")))


(defn select-customer []
  (json customer))

(defn create-account [email]
  
  )