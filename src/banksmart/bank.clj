(ns banksmart.bank
    (:require [clj-time.core :as t]))

(def ^:private empty-bank {})
(def bank (atom empty-bank))

(defn reset-bank
  "Returns bank to its original state."
  []
  (reset! bank empty-bank))

(defn ^:private create-account
  "Adds new empty account to bank identified by account-number."
  [account-number]
  (->
   bank
   (swap! #(assoc % account-number (sorted-map)))
   (get account-number)))


(defn add-account-transaction
  "Adds new bank transaction to account identified by account-number."
  [account-number description amount date]
  (when ((complement zero?) amount)
    (let [new_transaction {:description description :amount amount}]
      (as-> (create-account account-number) input
        (get input date [])
        (conj input new_transaction)
        (swap! bank assoc-in [account-number date] input)))))
