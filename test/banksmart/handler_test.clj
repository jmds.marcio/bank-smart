(ns banksmart.handler-test
  (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [cheshire.core :refer :all]
            [banksmart.handler :refer :all]))

(deftest test-app
  (testing "main route"
    (let [response (app (mock/request :get "/"))]
      (is (= (:status response) 200))
      (is (= (:body response) "Bank Smart - Wellcome"))))

  (testing "not-found route"
    (let [response (app (mock/request :get "/invalid"))]
      (is (= (:status response) 404)))))

(deftest test-customer-app
  (testing "customer not-found"
    (let [response (app (mock/request :get "/customer"))]
      (is (= (:status response) 200)))))

(deftest test-transaction-app
  (let [expected-response (generate-string {0 {:name "Joseph" :doc 25232142521 :email "jmds.marcio@gmail.com"}})
        response (app (mock/request :get "/customer" ) )]
    (is (= 200 (:status response)))
    (is (= "application/json; charset=utf-8" (get-in response [:headers "Content-Type"])))
    (is (= expected-response (:body response))))
  )